import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, AfterContentChecked } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import { FullCalendarComponent } from '@fullcalendar/angular';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {

  @ViewChild('fullCar', {static: true})
  public fullCar: FullCalendarComponent;

  title = 'stacked-bar-chart';
  data = [14, 25, 30, 31];
  chartData = [
    {
      groupName: 'South 32',
      background: '#CBDFBD',
      total: 727 + 1238 + 537 + 903,
      size: 0,
      data: [
        {
          size: 0,
          value: 727,
          label: 'item 1',
          background: '#B74F6F'
        },
        {
          size: 0,
          value: 1238,
          label: 'item 1',
          background: '#ADBDFF'
        },
        {
          size: 0,
          value: 537,
          label: 'item 1',
          background: '#237DFC'
        },
        {
          size: 0,
          value: 903,
          label: 'item 1',
          background: '#EAC435'
        },
      ]
    },
    {
      groupName: 'South 32',
      background: '#7C98B3',
      total: 938 + 671 + 192,
      size: 0,
      data: [
        {
          size: 0,
          value: 938,
          label: 'item 1',
          background: '#38686A'
        },
        {
          size: 0,
          value: 671,
          label: 'item 1',
          background: '#BA2C73'
        },
        {
          size: 0,
          value: 192,
          label: 'item 1',
          background: '#6D3B47'
        },
      ]
    },
    {
      groupName: 'South 32',
      background: '#536B78',
      total: 574 + 506 + 532,
      size: 0,
      data: [
        {
          size: 0,
          value: 574,
          label: 'item 1',
          background: '#345995'
        },
        {
          size: 0,
          value: 506,
          label: 'item 1',
          background: '#03CEA4'
        },
        {
          size: 0,
          value: 532,
          label: 'item 1',
          background: '#F75C03'
        },
      ]
    },
  ];

  chartDataFinal = [];

  chartItem = [];
  chartLabel = [];

  // calendar
  calendarPlugins = [dayGridPlugin, timeGridPlugin];

  events = [
    {
      title: 'BCH237',
      start: '2019-10-22T10:30:00',
      end: '2019-10-22T14:30:00',
      extendedProps: {
        department: 'BioChemistry'
      },
      description: 'Lecture',
      backgroundColor: '#B74F6F'
    },
    {
      title: '123AB',
      start: '2019-10-23T11:00:00',
      end: '2019-10-23T11:30:00',
      extendedProps: {
        department: 'BioChemistry'
      },
      description: 'Lecture',
      backgroundColor: '#237DFC'
    },
    {
      title: '123AB',
      start: '2019-10-22T11:00:00',
      end: '2019-10-22T11:35:00',
      extendedProps: {
        department: 'BioChemistry'
      },
      description: 'Lecture',
      backgroundColor: '#EAC435'
    },
    {
      title: '123AB',
      start: '2019-10-22T10:00:00',
      end: '2019-10-22T15:30:00',
      extendedProps: {
        department: 'BioChemistry'
      },
      description: 'Lecture',
      backgroundColor: '#ADBDFF'
    },
    {
      title: '123AB',
      start: '2019-10-22T10:00:00',
      end: '2019-10-22T15:30:00',
      extendedProps: {
        department: 'BioChemistry'
      },
      description: 'Lecture',
      backgroundColor: '#ADBDFF'
    },
    {
      title: '123AB',
      start: '2019-10-09T10:00:00',
      end: '2019-10-09T15:30:00',
      extendedProps: {
        department: 'BioChemistry'
      },
      description: 'Lecture',
      backgroundColor: '#ADBDFF'
    },
    {
      title: '123AB',
      start: '2019-10-09T10:00:00',
      end: '2019-10-09T15:30:00',
      extendedProps: {
        department: 'BioChemistry'
      },
      description: 'Lecture',
      backgroundColor: '#ADBDFF'
    },
    {
      title: '123AB',
      start: '2019-10-09T10:00:00',
      end: '2019-10-09T15:30:00',
      extendedProps: {
        department: 'BioChemistry'
      },
      description: 'Lecture',
      backgroundColor: '#ADBDFF'
    },
    {
      title: '123AB',
      start: '2019-10-09T10:00:00',
      end: '2019-10-09T15:30:00',
      extendedProps: {
        department: 'BioChemistry'
      },
      description: 'Lecture',
      backgroundColor: '#ADBDFF'
    },
    {
      title: '123AB',
      start: '2019-10-09T10:00:00',
      end: '2019-10-09T15:30:00',
      extendedProps: {
        department: 'BioChemistry'
      },
      description: 'Lecture',
      backgroundColor: '#ADBDFF'
    },
    {
      title: '123AB',
      start: '2019-10-09T10:00:00',
      end: '2019-10-09T15:30:00',
      extendedProps: {
        department: 'BioChemistry'
      },
      description: 'Lecture',
      backgroundColor: '#ADBDFF'
    },
    {
      title: '123AB',
      start: '2019-10-09T10:00:00',
      end: '2019-10-09T15:30:00',
      extendedProps: {
        department: 'BioChemistry'
      },
      description: 'Lecture',
      backgroundColor: '#ADBDFF'
    },
    {
      title: '123AB',
      start: '2019-10-09T10:00:00',
      end: '2019-10-09T15:30:00',
      extendedProps: {
        department: 'BioChemistry'
      },
      description: 'Lecture',
      backgroundColor: '#ADBDFF'
    },
    {
      title: '123AB',
      start: '2019-10-09T10:00:00',
      end: '2019-10-09T15:30:00',
      extendedProps: {
        department: 'BioChemistry'
      },
      description: 'Lecture',
      backgroundColor: '#ADBDFF'
    },
  ];
  constructor(private cdref: ChangeDetectorRef) { }

  public ngOnInit(): void {
    // this.chartItem = this.chartData.reduce((rs, item) => {
    //   const data = item.data.map(i => {
    //     return {
    //       ...i,
    //       borderColor: this.convertHex(i.background, 40)
    //     };
    //   });
    //   return [...rs, ...data];
    // }, []);
    // console.log(this.chartItem);
  }

  public changeDate() {
    console.log('changeDate');
    this.fullCar.calendar.gotoDate('2019-10-23')
  }

  public ngAfterViewInit() {
    setTimeout(() => {
      this.render();
      this.renderLabel();
    }, 500);
  }

  public renderLabel() {
    const total = this.chartData.reduce((sum, item) => {
      return sum + item.total;
    }, 0);

    this.chartData.forEach(item => {
      item.size = (item.total / total) * 100;
    });
  }

  // Get chart width
  public render() {
    console.log(this.chartItem);

    const dataSum = this.chartItem.reduce((sum, item) => {
      return sum + item.value;
    }, 0);

    // calculate size chart item
    this.chartItem.forEach(item => {
      item.size = (item.value / dataSum) * 100;
    });

    // this.chartDataFinal = this.chartData.map(item => {
    //   return {
    //     ...item,
    //     size: (item.value / dataSum) * 100,
    //   }
    // })

    console.log(this.chartItem);

    this.cdref.detectChanges();
  }

  public convertHex(hex: string, opacity: number) {
      const hexNew = hex.replace('#', '');
      const r = parseInt(hexNew.substring(0, 2), 16);
      const g = parseInt(hexNew.substring(2 , 4), 16);
      const b = parseInt(hexNew.substring(4, 6), 16);
      const result = 'rgba(' + r + ',' + g + ',' + b + ',' + opacity / 100 + ')';
      return result;
  }
}
